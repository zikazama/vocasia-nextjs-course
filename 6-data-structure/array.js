// cara penulisan array secara langsung
const arr = [100, 200, 300];
// cara penulisan inisialisasi array yang tidak diketahui jumlahnya
const arr2 = [];
// cara menambahkan array secara manual
arr2[0] = 100;
arr2[1] = 200;
arr2[2] = 300;
// cara menambahkan nilai baru pada array di posisi terakhir
arr2.push(400);