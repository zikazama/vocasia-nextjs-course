export default function Static({ comments }) {
  return (
    <ul>
      {comments.map((comment) => (
        <li>{comment.body}</li>
      ))}
    </ul>
  );
}

export async function getStaticProps() {
  // Fetch data from external API
  const res = await fetch("https://jsonplaceholder.typicode.com/posts/1/comments");
  const comments = await res.json();
  // Pass data to the page via props
  return { props: { comments } };
}
