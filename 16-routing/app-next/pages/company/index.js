export default function listCompany() {
  return (
    <>
      <h3>List Of Company</h3>
      <ul>
        <li>
          <a href="/company/geulante">
            <span>PT. Geulante</span>
          </a>
        </li>
        <li>
          <a href="/company/cerape">
            <span>PT. Cerape</span>
          </a>
        </li>
        <li>
          <a href="/company/leumang">
            <span>PT. Leumang</span>
          </a>
        </li>
      </ul>
    </>
  )
}
