import { useRouter } from 'next/router'
export default function companyDetail() {
  const router = useRouter()
  const { company } = router.query
  return (
    <>
      <h3>detail of {company}</h3>
    </>
  )
}