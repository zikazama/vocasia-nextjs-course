// Harus selalu import React
// dan karena di sini kita menggunakan Component
// harus de-structuring juga Component-nya
import React, { Component } from 'react';

// di sini kita akan membuat sebuah button dengan
// react class component
class ButtonClass extends Component {
  // di sini kita akan render sebuah button dengan jsx
  render() {
    return <button>Button Class</button>;
  }
}

// jangan lupa di-export agar bisa digunakan
export default ButtonClass;
