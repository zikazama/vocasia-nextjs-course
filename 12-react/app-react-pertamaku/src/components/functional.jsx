// Harus selalu import React
import React from 'react';

// di sini kita akan membuat sebuah button dengan
// react functional component
const ButtonFunctional = () => {
  // secara default ini sudah ada render()
  // jadi tidak perlu dituliskan lagi
  return <button>Button Functional</button>;
};

export default ButtonFunctional;
