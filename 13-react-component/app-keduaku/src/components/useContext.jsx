// Import useContext
import React, { useContext } from 'react';
// Import context yang ingin digunakan
import NamaContext from "./context.jsx";

export default function ChildComponent() {
  // Di sini ChildComponent meminta value dari context 
  // yang didefinisikan oleh ParentComponent
  const valContext = useContext(NamaContext);
}