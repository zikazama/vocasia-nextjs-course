import React from 'react';
 
function Parent() {
  return <Child name="Nusantech Academy" />
}
 
function Child(props){
  return (
    <div>
      <h1>Hello {props.name}</h1>
    </div>
  )
}
 
export default Parent;