import { useState } from "react";
import React from 'react'

// functional state
function Aplikasi() {
  const [increment, setIncrement] = useState(0);

  function buttonNambahClickHandler() {
    console.log(increment);
    // di sini karena kita ketahui bahwa setIncrement adalah fungsi
    // untuk memutasi (mengubah) si increment menjadi nilai baru
    setIncrement(increment + 1);
  }

  return (
    <>
      <span style={{ marginRight: '0.25em' }}>{increment}</span>
      <span>
        <button onClick={buttonNambahClickHandler}>Nambah</button>
      </span>
    </>
  );
}

// component state
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {name: "Fauzi"};
  }
  render() {
    return (
      <div>
        <h1>Hello {this.state.name}</h1>
      </div>
    );
  }
}
 
export default App