import React from 'react';
 
function Parent() {
  return <Child name="Vocasia" />
}

function clickHandler(){
    alert('Button clicked');
}
 
function Child(props){
  return (
    <div>
      <button onClick={clickHandler}></button>
    </div>
  )
}
 
export default Parent;