import React, { useEffect, useState } from 'react';

function ToDo() {
const [todos, setTodos] = useState([]);
  
  useEffect(
    () => {
      let textTitle = 'Todos: ' + todos.length;
      console.log(textTitle);
      document.title = textTitle;
    },
    [todos]
  );

  return (
<>{...todos}</>
    )
}