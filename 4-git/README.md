repo(sitory) : Folder project kita berada
remote : sumber lain yang memiliki repo, di luar komputer kita
clone : mengambil repo dari sumber remote
commit : menyimpan / merekam (snapshot) kode dari repo
branch : cabang dari sebuah commit
checkout : proses untuk berpindah dari satu commit / satu cabang ke commit / cabang lainnya
merge : proses untuk menggabungkan branch
push : mengirimkan commit ke repo (biasanya remote repo)
pull : mengambil commit dari repo (biasanya remote repo)
Istilah dalam GitHub
fork(ing) : membuat kopi / duplikat dari repo publik orang lain (beserta rekam jejaknya)
(sebagai jembatan antara repo original dengan repo yang diduplikat)
pull request : permintaan untuk menggabungkan (merge) kode
(umumnya digunakan ketika ingin menggabungkan kode repo kita ke repo sumber fork)




