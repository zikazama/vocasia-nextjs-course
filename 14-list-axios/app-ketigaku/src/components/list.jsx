// POST Request
import axios from "axios";
import { useEffect, useState } from "react";

function ListNama() {
  const [users, setUser] = useState([]);

  async function fetchData() {
    const result = await axios({
      method: "GET",
      url: "https://jsonplaceholder.typicode.com/users",
    });
    setUser(result.data);
  }

  useEffect(() => {
   fetchData();
  }, [users]);

  return (
    <>
    {users.map((user) => (<li>{user.name}</li>))}
    </>
  );
}

export default ListNama;
