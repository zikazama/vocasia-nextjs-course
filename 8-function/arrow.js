// cara penulisan arrow function
const arrowFunction = (param1, param2) => {
  // kode di sini //
  console.log('menampilkan param1: ' + param1);
  console.log('menampilkan param2: ' + param2);
};

// invoke fungsi
arrowFunction("arg1", "arg2");
