class Smartphone {
    constructor(cpu, memory, storage, isNFCEnabled = false) {
      this.cpu = cpu;
      this.memory = memory;
      this.storage = storage;
      this.isNFCEnabled = isNFCEnabled;
  
      this.kiri;
      this.kanan;
    }
  
    bisaNyala() {}
    bisaDisentuh() {}
  
    swipeLeft(kiri) {
      this.kiri = kiri;
      // Harus ada yang dikembalikan (this) supaya bisa mengembalikan
      // si instance smartphone yang dibuat untuk digunakan kembali
      return this;
    }
  
    swipeRight(kanan) {
      this.kanan = kanan;
      // Harus ada yang dikembalikan (this) supaya bisa mengembalikan
      // si instance smartphone yang dibuat untuk digunakan kembali
      return this;
    }
  
    swipeAll() {
      console.log(`kiri: ${this.kiri} dan kanan: ${this.kanan}`);
      // Apakah masih butuh kembalian?
      // Tentunya tidak ! karena ini sudah paling terakhir bukan?
      // (tergantung kebutuhan)
    }
  
    showSpec() {
      return `CPU: ${this.cpu}, Memory: ${this.memory}, Storage: ${this.storage}`;
    }
  }
  
  let samsulS22 = new Smartphone('Ex-nose 9820', '8 GB', '128 GB', true);
  
  // Cara mengunakan method chaining
  samsulS22.swipeLeft('Hello').swipeRight('World').swipeAll();
  
  module.exports = Smartphone;
