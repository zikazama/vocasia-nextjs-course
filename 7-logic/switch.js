let kondisi = 20;
switch (kondisi) {
  case 20:
    // kondisional 1
    console.log("Masuk kondisional 1");
    break;
  case 30:
    // kondisional 2
    console.log("Masuk kondisional 2");
    break;
  default:
    // kondisional 3
    console.log("Masuk kondisional 3");
    break;
}
