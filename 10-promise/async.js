// Cara menggunakan fungsi gabungKata (Promise)

function gabungKata(kata) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(kata);
        }, 1000);
    });
}

async function utama(){
    const hasil = await gabungKata("Hello");
    console.log(hasil);
}

utama();