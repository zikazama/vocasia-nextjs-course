// Cara menggunakan fungsi gabungKata (Promise)

function gabungKata(kata) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(kata);
        }, 1000);
    });
}

gabungKata("Hello") // Kondisi berhasil (resolved)
  .then(
    // fnCallback resolve
    (dataHasilResolve) => {
      console.log(dataHasilResolve);
    }
  )
  // Kondisi gagal (rejected)
  .catch(
    // fnCallback reject
    (rejectMessage) => {
      console.log(rejectMessage);
    }
  );

