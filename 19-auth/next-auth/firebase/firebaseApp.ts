// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB4BTruAAfg0v3InzIns3tN8e0PsFGUp8c",
  authDomain: "zyborg-puekqd.firebaseapp.com",
  databaseURL: "https://zyborg-puekqd.firebaseio.com",
  projectId: "zyborg-puekqd",
  storageBucket: "zyborg-puekqd.appspot.com",
  messagingSenderId: "1040971975011",
  appId: "1:1040971975011:web:3657d9fbcf947bd6bd2b56"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);

// Export function to initialize firebase.
export const initFirebase = () => {
  return app;
};
