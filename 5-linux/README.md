su : untuk masuk ke super user
login : untuk masuk ke user tertentu
pwd : untuk lihat directory
cd : untuk masuk ke direktori
ls : check isi file pada folder
cat : untuk check isi file
mv : untuk memindahkan file
cp : untuk menduplikasi file
mkdir : membuat folder
rmdir : menghapus folder
rm : menghapus file
touch : membuat file baru
chown : untuk mengubah hak akses
ping : untuk cek koneksi