// karena fungsiKesatu yang harus menunggu fungsiKedua
// maka callbacknya akan dimasukkan ke sini
function fungsiKesatu(fnCallback) {
  setTimeout(() => {
    // ketika timeout sudah selesai console log ini akan berjalan console.log('satu');
    // baru setelah itu fnCallback di-invoke
    fnCallback();
  }, 1000);
}
function fungsiKedua() {
  console.log("dua");
}
fungsiKesatu(fungsiKedua);
